//
//  Physics.h
//  CatNap
//
//  Created by Brandon Levasseur on 1/21/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

typedef NS_OPTIONS(uint32_t, CNPhysicsCategory)
{
    CNPhysicsCategoryBoard  = 1 << 0,
    CNPhysicsCategoryBrick  = 1 << 1,
    CNPhysicsCategoryEdge   = 1 << 2,
    CNPhysicsCategoryBall   = 1 << 3
};

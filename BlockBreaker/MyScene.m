//
//  MyScene.m
//  BlockBreaker
//
//  Created by Brandon Levasseur on 1/28/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

#import "MyScene.h"
#import "Physics.h"
#import "SKTAudio.h"
#import "SKTUtils.h"

@interface MyScene () <SKPhysicsContactDelegate>

@end

@implementation MyScene
{
    SKNode *_gameNode;
    SKShapeNode *_paddle;
    int _currentLevel;
    CGPoint _velocity;
    NSTimeInterval _lastUpdateTime;
    NSTimeInterval _dt;
    CGPoint _lastTouchPosition;
}

static const float BOARD_MOVE_POINTS_PER_SEC = 500.0;

-(instancetype)initWithSize:(CGSize)size
{
    if (self = [super initWithSize:size]) {
        [self initializeScene];
    }
    return self;
}

- (void)initializeScene
{
    self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
    self.physicsWorld.contactDelegate = self;
    self.physicsBody.categoryBitMask = CNPhysicsCategoryEdge;
    
    SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"background"];
    bg.position = CGPointMake(self.size.width/2, self.size.height/2);
    [self addChild:bg];
    
    _gameNode = [SKNode node];
    [self addChild:_gameNode];
    
    _currentLevel = 1;
    [self setUpLevel:_currentLevel];
    
    self.physicsWorld.gravity = CGVectorMake(0, 0);
}


-(void)update:(CFTimeInterval)currentTime {
    SKNode* ball = [self childNodeWithName:@"Ball"];
    static int maxSpeed = 1000;
    float speed = sqrt(ball.physicsBody.velocity.dx*ball.physicsBody.velocity.dx + ball.physicsBody.velocity.dy * ball.physicsBody.velocity.dy);
    if (speed > maxSpeed) {
        ball.physicsBody.linearDamping = 0.4f;
    } else {
        ball.physicsBody.linearDamping = 0.0f;
    }
}


- (void)setUpLevel:(int)levelNum
{
    
    _paddle = [self createPaddleNode];
    _paddle.position = CGPointMake(CGRectGetMidX(self.frame) - CGRectGetWidth(_paddle.frame)/2, CGRectGetMidY(_paddle.frame));
    [_gameNode addChild:_paddle];
    
    SKShapeNode *ballNode = [self createBallNode];
    ballNode.position = CGPointMake(CGRectGetMidX(self.frame), _paddle.frame.size.height + ballNode.frame.size.height);
    [_gameNode addChild:ballNode];
    
    [ballNode.physicsBody applyImpulse:CGVectorMake(0, 20)];
    
}

- (SKShapeNode *)createBallNode
{
    SKShapeNode *ball = [SKShapeNode node];
    
    NSInteger radius = 15;
    
    CGMutablePathRef circlePath = CGPathCreateMutable();
    CGPathAddArc(circlePath, nil, 0, 0, radius, 0, M_PI*2, YES);
    
    ball.path = circlePath;
    ball.name = @"Ball";
    ball.strokeColor = [SKColor colorWithRed:1.0 green:0 blue:0 alpha:0.5];
    ball.fillColor = [SKColor colorWithRed:1.0 green:0 blue:1 alpha:0.5];
    ball.lineWidth = 1.0;
    ball.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:radius];
    ball.physicsBody.restitution = 1;
    ball.physicsBody.friction = 0;
    ball.physicsBody.linearDamping = 0;
    ball.physicsBody.allowsRotation = NO;
    ball.physicsBody.usesPreciseCollisionDetection = YES;
    
    ball.physicsBody.categoryBitMask = CNPhysicsCategoryBall;
    
    ball.physicsBody.collisionBitMask = CNPhysicsCategoryEdge | CNPhysicsCategoryBoard;
    
    ball.physicsBody.contactTestBitMask = CNPhysicsCategoryEdge;
    
    return ball;
    
    
}

- (SKShapeNode *)createPaddleNode
{
    SKShapeNode *paddle = [SKShapeNode node];
    
    CGPathRef squarePath = CGPathCreateWithRect(CGRectMake(0, 0, 100, 25), nil);
    
    paddle.path = squarePath;
    paddle.strokeColor = [SKColor colorWithRed:1.0 green:0 blue:0 alpha:0.5];
    paddle.fillColor = [SKColor colorWithRed:1.0 green:0 blue:1 alpha:0.5];
    paddle.lineWidth = 1.0;
    
    paddle.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:squarePath];
    
    CGPathRelease(squarePath);
    
    paddle.physicsBody.categoryBitMask = CNPhysicsCategoryBoard;
    
    
    paddle.physicsBody.collisionBitMask = CNPhysicsCategoryEdge;
    
    paddle.physicsBody.contactTestBitMask = CNPhysicsCategoryEdge;
    paddle.physicsBody.dynamic = NO;
    
    return paddle;
}


-(void)touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event {
    
    
    UITouch* touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInNode:self];
    CGPoint previousLocation = [touch previousLocationInNode:self];
    int paddleX = _paddle.position.x + (touchLocation.x - previousLocation.x);
    paddleX = MAX(paddleX, 0);
    paddleX = MIN(paddleX, self.size.width - CGRectGetWidth(_paddle.frame));
    _paddle.position = CGPointMake(paddleX, _paddle.position.y);
    
}



@end

//
//  AppDelegate.h
//  BlockBreaker
//
//  Created by Brandon Levasseur on 1/28/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
